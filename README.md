Html5ProjSE
======
Html 5 Project - Simple Example
Html 或 Html 5 前端標籤相關代碼收錄

Feature
------

1. [FileAPI](https://rhzenoxs.github.io/Html5ProjSE/src/FileAPI/FileApi.html)
2. [富文本編輯器](https://rhzenoxs.github.io/Html5ProjSE/src/Editor/Editor.html)
    + Alt + 1  開啟側邊欄
    + Ctrl 長按 說明對話視窗
3. CustomElement
4. BasisUI
    + List
    + Select
    + Checkbox
